__author__ = "Martin Fiser"
__credits__ = "Martin Fiser, 2017, Twitter: @VFisa"
__source__ = "Facebook Prophet library"
__link__ = "https://facebookincubator.github.io/prophet/"


# Import Libraries
import json
import logging
import os
import sys
import warnings
from time import gmtime, strftime
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.cbook
import numpy as np
import pandas as pd
from fbprophet import Prophet
from keboola import docker
import logging_gelf
import logging_gelf.formatters
import logging_gelf.handlers

# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S"
        )
logger = logging.getLogger()
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
    )
logging_gelf_handler.setFormatter(logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)
# Removes the initial stdout logging - for production
# logger.removeHandler(logger.handlers[0])


# Get proper list of tables
cfg = docker.Config("/data/")
in_tables = cfg.get_input_tables()
logging.debug("IN tables mapped: "+str(in_tables))
out_tables = cfg.get_expected_output_tables()
logging.debug("OUT tables mapped: "+str(out_tables))

# Get parameters
params = cfg.get_parameters()
COLUMN_DATE = cfg.get_parameters()["column_date"]
COLUMN_VALUES = cfg.get_parameters()["column_values"]
PERIOD = cfg.get_parameters()["predict_period"]
FREQUENCY = cfg.get_parameters()["predict_frequency"]
DEBUG = cfg.get_parameters()["debug_mode"]
INTERVAL_WIDTH = cfg.get_parameters()["uncertainty_trend"]
MCMC_SAMPLES = cfg.get_parameters()["uncertainty_seasonality"]
FUTURE_CAP = cfg.get_parameters()["future_cap"]
FUTURE_FLOOR = cfg.get_parameters()["future_floor"]
GENERATE_CHARTS = cfg.get_parameters()["generate_charts"]
CHANGEPOINT_PRIOR_SCALE = cfg.get_parameters()["trend_flexibility"]
CHANGEPOINTS = cfg.get_parameters()["changepoints"]
HOLIDAYS = cfg.get_parameters()["holidays"]
OUTLIERS = cfg.get_parameters()["outliers"]
specs = [COLUMN_DATE, COLUMN_VALUES, PERIOD, FREQUENCY]
DATE_SNIP = strftime("%Y-%m-%d-%H-%M-%S", gmtime())

"""
edits = [{
            "type": "log",
            "name": "true"
        },
        {
            "type": "nan",
            "name": "500"
        }
        ]
"""


def load_tables(in_tables, out_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.debug("Data table: " + str(in_name))
    logging.debug("Input table source: " + str(in_destination))

    table = out_tables[0]
    out_name = table["full_path"]
    out_Destination = table["source"]
    logging.debug("Output table: " + str(out_name))
    logging.debug("Output table destination: " + str(out_Destination))

    return in_name, out_name


def output_file(output_model, file_out="data.csv"):
    """
    Save output data as CSV (pandas)
    """

    # with open(file_out, "w", encoding="utf-8") as csvfile:
    with open(file_out, "w") as csvfile:  # P2
        output_model.to_csv(csvfile, index=False)
        logging.info("Forecast file produced.")
    csvfile.close()


def input_file(file_in):
    """
    Read input data as CSV DataFrame (pandas)
    """

    try:
        frame = pd.read_csv(file_in, encoding="utf-8")
        logging.info("Input file read.")
    except Exception as e:
        logging.error("Could not read input file! Exit")
        logging.error(str(e))
        sys.exit(1)

    return frame


def test_model(data, specs):
    """
    Test model properties:
     - There needs to be one data column and one data columns.
     - System looks for columns names within table.
    Test input parameters:
     - specs = [COLUMN_DATE, COLUMN_DATE, PERIOD, FREQUENCY]
    Test additional params:
     - INTERVAL_WIDTH
     - MCMC_SAMPLES
     - FUTURE_CAP
     - FUTURE_FLOOR
     - CHANGEPOINT_PRIOR_SCALE
     - CHANGEPOINTS
    """

    # Test csv file - Column validation check
    try:
        # first two parameters are column names
        for a in specs[:2]:
            position = data.columns.get_loc(a)
        logging.info("Column parameters correct")
        logging.info("Date column: " + str(specs[0]))
        logging.info("Value column: " + str(specs[1]))
    except:
        logging.error("Input table and parameters are not correct! Exit.")
        sys.exit(1)

    # Use only date and the value column based on selection
    df = model[[specs[0], specs[1]]].copy()
    df.columns = ["ds", "y"]
    dates = set(df["ds"])

    # Test csv file - date conversion
    try:
        df["ds"] = pd.to_datetime(df["ds"])
    except Exception as e:
        logging.error("Could not convert date column! Exit.")
        logging.error(str(e))
        sys.exit(1)
    
    # Test csv file - numeric conversion
    try:
        df["y"] = pd.to_numeric(df["y"], errors="coerce")
    except Exception as e:
        logging.error("Could not convert value column to number! Exit")
        logging.error(str(e))
        sys.exit(1)

    # period value validation
    try:
        number = specs[2]
        assert type(number) is int
        logging.info("Value for prediction period selected: "+str(number))
        period_validated = number
    except AssertionError:
        logging.warning(
            "Value for period is not correct, replacing by standard 90 days.")
        period_validated = 12
    
    # period frequency validation
    freq = specs[3]
    # freq: Any valid frequency for pd.date_range
    # http://pandas.pydata.org/pandas-docs/stable/timeseries.html#offset-aliases
    # TODO: implement more stuff here
    if freq == "":
        logging.warning("Value for frequency is not set, using standard days.")
        freq_validated = "D"
    else:
        try:
            freq_allowed = ["H", "D", "W", "M", "MS", "Q", "QS", "A", "AS"]
            assert any(freq in s for s in freq_allowed)
            logging.info("Value for prediction frequency selected: "+str(freq))
            freq_validated = freq
        except AssertionError:
            logging.warning(
                "Value for frequency is not correct, using standard days.")
            logging.warning(
                "If you want to include your frequency specification, please contact author.")
            freq_validated = "D"

    # interval width = Uncertainty in the trend
    if INTERVAL_WIDTH == "":
        logging.warning(
            "Value for Interval width (Uncertainty in the trend) not set, using defaults.")
        interval_width_validated = 0.80
    else:
        try:
            interval_width_validated = float(INTERVAL_WIDTH)
            logging.info("Uncertainty in the trend selected: " + str(interval_width_validated))
        except:
            logging.error(
            "Could not convert Interval width (Uncertainty in the trend) to decimal, using defaults.")
            interval_width_validated = 0.80

    # mcmc_samples = Uncertainty in seasonality
    if MCMC_SAMPLES == "":
        logging.warning(
            "Value for MCMC Samples (Uncertainty in seasonality) not set, using defaults.")
        mcmc_samples_validated = 0
    else:
        try:
            mcmc_samples_validated = int(MCMC_SAMPLES)
            logging.warning(
            "MCMC Samples (Uncertainty in seasonality) manually set, this will take some time.")
        except:
            logging.error(
            "Could not convert MCMC Samples (Uncertainty in seasonality) to integer, using defaults.")
            mcmc_samples_validated = 0

    # Future cap
    if FUTURE_CAP == "":
        logging.warning("Value for Future Cap not set, skipping.")
        future_cap_validated = ""
    else:
        try:
            future_cap_validated = float(FUTURE_CAP)
            logging.info("Carrying capacity selected: " + str(future_cap_validated))
        except:
            logging.error(
            "Could not convert Future Cap to decimal, skipping.")
            future_cap_validated = ""

    # Future floor
    if FUTURE_FLOOR == "":
        logging.warning("Value for Future Floor not set, skipping.")
        future_floor_validated = ""
    else:
        try:
            future_floor_validated = float(FUTURE_FLOOR)
            logging.info("Saturating Minimum selected: " + str(future_floor_validated))
        except:
            logging.error(
            "Could not convert Future Floor value to decimal, skipping.")
            future_floor_validated = ""

    # Adjusting trend flexibility
    if CHANGEPOINT_PRIOR_SCALE == "":
        logging.warning(
            "Value for Trend flexibility (Changepoint prior scale) not set, skipping.")
        changepoint_prior_scale_validated = ""
    else:
        try:
            changepoint_prior_scale_validated = float(CHANGEPOINT_PRIOR_SCALE)
            logging.info("Trend flexibility (Changepoint prior scale) selected: " + str(changepoint_prior_scale_validated))
        except:
            logging.error(
            "Could not convert Trend flexibility (Changepoint prior scale) value to decimal, skipping.")
            changepoint_prior_scale_validated = 0.5

    # Manual changepoints
    if len(CHANGEPOINTS) == 0:
        logging.warning("No manual Changepoints selected, skipping.")
        changepoints_validated = []
    else:
        try:
            changepoints_validated = []
            for a in CHANGEPOINTS:
                if a in dates:
                    changepoints_validated.append(a)
                else:
                    logging.info("Changepoint: "+str(a)+" could not be found in data, skipping.")
            logging.info("Using manual Changepoints.")
            logging.info(changepoints_validated)
        except:
            logging.error("Could not used selected Changepoints, skipping.")
            changepoints_validated = []
    
    # Holidays
    if len(HOLIDAYS) == 0:
        logging.warning("No Holidays specified, skipping.")
        holidays_validated = []
    else:
        try:
            holidays_validated = HOLIDAYS
            logging.info("Using manual Holidays.")
            logging.info(holidays_validated)
        except:
            logging.error("Could not used selected Holidays, skipping.")
            holidays_validated = []

    # Outliers
    if len(OUTLIERS) == 0:
        logging.warning("No Outliers specified, skipping.")
        outliers_validated = []
    else:
        try:
            outliers_validated = OUTLIERS
            logging.info("Using manual Outliers.")
            logging.info(outliers_validated)
        except:
            logging.error("Could not used selected Outliers, skipping.")
            outliers_validated = []

    # final specs
    specs_validated = [
        specs[0],
        specs[1],
        period_validated,
        freq_validated,
        interval_width_validated,
        mcmc_samples_validated,
        future_cap_validated,
        future_floor_validated,
        changepoint_prior_scale_validated,
        changepoints_validated,
        holidays_validated,
        outliers_validated
        ]

    return specs_validated


def prediction(data, specs_final):
    """
    Steps:
    Input columns are converted, perform forecast, returns dataframe.
    1 - log-transform the y variable
    2 - Fit the model by instantiated a new Prophet object.
    Any settings to the forecasting procedure are passed into the constructor.
    Then you call its fit method and pass in the historical dataframe.
    3 - get a suitable dataframe that extends into the future
    a specified number of days using the helper method
    4 - The predict method will assign each row in future
    a predicted value which it names yhat. 
    The forecast object is a new dataframe that includes
    a column yhat with the forecast and uncertainty intervals.
    
    Parameters explanation:
    # Trend flexibility:
    If the trend changes are being overfit (too much flexibility) or
    underfit (not enough flexibility), you can adjust the strength.
     - By default, this parameter is set to 0.05
     - Increase will make the trend more flexible
     - Decrease will make the trend less flexible
    # Uncertainty Intervals:
    Allowing higher flexibility in the rate, by increasing changepoint_prior_scale,
    will increase the forecast uncertainty. This is because if we model more rate 
    changes in the history then we will expect more in the future, and makes the 
    uncertainty intervals a useful indicator of overfitting

    # Holidays
    https://facebookincubator.github.io/prophet/docs/holiday_effects.html
    # Outliers (user specified date range with wrong data)
    https://facebookincubator.github.io/prophet/docs/outliers.html
    """

    # Figure out values and setup
    column_date = specs_final[0]
    column_value = specs_final[1]
    period = specs_final[2]
    frequency = specs_final[3]
    # Uncertainty in the trend
    interval_width = specs_final[4]
    # Uncertainty in seasonality
    mcmc_samples = specs_final[5]
    # Values caps
    future_cap = specs_final[6]
    future_floor = specs_final[7]
    # Trend flexibility
    changepoint_prior_scale = specs_final[8]
    # Explicit changepoints
    changepoints = specs_final[9]
    # Holidays
    holidays = specs_final[10]
    # Outliers
    outliers = specs_final[11]
    # Date format setup
    date_format = "%Y-%d-%m %H:%M:%S"

    # model fit
    df = model[[column_date, column_value]].copy()
    df.columns = ["ds", "y"]

    # Date conversion
    df["ds"] = pd.to_datetime(df["ds"])
    #df["ds"] = df["ds"].astype("date")
    #df["ds"] = pd.to_datetime(df["ds"], format=date_format)

    # Numeric conversion
    df["y"] = pd.to_numeric(df["y"], errors="coerce")
    #df["y"] = df["y"].astype("float").fillna(0.0)
    # df.convert_objects(convert_numeric=True)
    #df["y"] = pd.to_numeric(df["y"], errors="coerce", downcast="signed")

    # Sort values - just in case
    df = df.sort_values("ds", axis=0, ascending=True,
                        inplace=False, kind="quicksort", na_position="last")

    # Execute forecast
    try:
        # Step 0 (Save original values before log)
        df["y_origin"] = df["y"]

        # Step 1
        try:
            df["y"] = np.log(df["y"])
            log_applied = True
        except Exception as e:
            logging.warning("Could not apply log function on the data. Check data quality!")
            logging.warning(str(e))
            log_applied = False

        """
        # Outliers (TODO)
        https://facebookincubator.github.io/prophet/docs/outliers.html
        df.loc[(df['ds'] > '2010-01-01') & (df['ds'] < '2011-01-01'), 'y'] = None
        """

        p_kwargs = {
            "interval_width": interval_width
            }
        f_kwargs = {
                "periods": period,
                "freq": frequency
            }
        
        # Adding extra parameters if present
        if future_cap != "":
            df["cap"] = future_cap
            p_kwargs["growth"] = "logistic"
        if future_floor != "":
            df["future_floor"] = future_floor
            p_kwargs["growth"] = "logistic"
        if changepoint_prior_scale != "":
            p_kwargs["changepoint_prior_scale"] = changepoint_prior_scale
        if len(changepoints) != 0:
            p_kwargs["changepoints"] = changepoints
        if mcmc_samples != 0:
            p_kwargs["mcmc_samples"] = mcmc_samples
        if holidays != []:
            p_kwargs["holidays"] = holidays
        
        # Step 2
        m = Prophet(**p_kwargs)
        m.fit(df)
        
        # Step 3
        future = m.make_future_dataframe(**f_kwargs)
        
        # Adding limits
        if future_cap != "":
            future["cap"] = future_cap
        if future_floor != "":
            future["floor"] = future_floor
        
        # Step 4
        forecast = m.predict(future)

        # Converting forecast to original values (exp)
        if log_applied == True: 
            try:
                ## TODO: http://pythondata.com/forecasting-time-series-data-with-prophet-part-2/
                forecast_data_orig = forecast
                forecast_data_orig["yhat"] = np.exp(forecast_data_orig["yhat"])
                forecast_data_orig["yhat_lower"] = np.exp(forecast_data_orig["yhat_lower"])
                forecast_data_orig["yhat_upper"] = np.exp(forecast_data_orig["yhat_upper"])
                try:
                    # Original data is drawn on the forecast but the black dots
                    # are log-transformed original ‘y’ data
                    logging.info("Applying original y values (ex-log).")
                    df["y_log"] = df["y"] #copy the log-transformed data to another column
                    df["y"] = df["y_origin"] #copy the original data to "y"
                except:
                    logging.warning("Could not convert original dots back, so the chart will look weird.")
            except:
                logging.warning("Could not convert forecast to exp numbers back, keeping the log values.")
                forecast_data_orig = forecast
        else:
            logging.warning("No log function applied, keeping values as it is.")
            forecast_data_orig = forecast
        
        # Output values if debug mode selected
        if DEBUG == True:
            print("\nModel parameters used:")
            print(p_kwargs)
            print(f_kwargs)
            print("\nLog function applied: "+str(log_applied))
            print("\nDataframe")
            print(df.tail())
            print("\nChangepoints used:")
            print(m.changepoints)
            print("\nForecast LOG data (tail):")
            """
            DOES NOT WORK
            # just print forecast with log values
            print("\nForecast with LOG values:")
            print(forecast[["ds", "t", "yhat", "yhat_lower", "yhat_upper"]].tail())
            print(forecast[["ds", "t", "yhat", "yhat_lower", "yhat_upper"]].tail())
            print("\nForecast with original values (tail):")
            print(forecast_data_orig[["ds", "t", "yhat", "yhat_lower", "yhat_upper"]].tail())
            """


    except Exception as e:
        logging.error("Could not generate the forecast! Exit.")
        logging.error(str(e))
        sys.exit(0)

    ## data ranges
    minimum = max(model.index)
    maximum = max(forecast.index)
    #start = model.iloc[minimum]
    #end = forecast.iloc[maximum]
    missing_dates = forecast[minimum:]
    start_forecast = missing_dates.iloc[0]["ds"]
    end_forecast = missing_dates.iloc[-1]["ds"]
    if DEBUG == True:
        print("Forecast start: "+str(start_forecast))
        print("Forecast end: "+str(end_forecast))

    # Charting
    if GENERATE_CHARTS == True:
        generate_charts(m, forecast, start_forecast, end_forecast)
    else:
        logging.warning("Chart creation was not selected. Skipping.")

    return forecast_data_orig


def generate_charts(f_model, f_forecast, f_start, f_end):
    """
    Generate spiced up charts:
     - forecast
     - components
     - changepoints
     - deltas
    """

    # plt.ioff() # turn of interactive plotting mode
    matplotlib.style.use("ggplot")

    try:
        # Standard forecast
        fig = f_model.plot(f_forecast, uncertainty=True)
        ax = fig.add_subplot(111)
        ax.set_ylabel(COLUMN_VALUES)
        ax.set_xlabel(COLUMN_DATE)
        #ax.legend(loc="center left", bbox_to_anchor=(1.0, 0.5))
        #ax.plot(f_model[COLUMN_DATE].values, f_forecast['trend'], ls='-', c="orange")
        plt.axvspan(f_start, f_end, color="red", alpha=0.1)
        save_charts(fig, "forecast")
        plt.close(fig)

        # Components
        fig = f_model.plot_components(f_forecast)
        save_charts(fig, "components")
        plt.close(fig)

        # changepoints
        fig = f_model.plot(f_forecast)
        for changepoint in f_model.changepoints:
            plt.axvline(changepoint, ls="--", lw=1)
        ax = fig.add_subplot(111)
        ax.set_ylabel(COLUMN_VALUES)
        ax.set_xlabel(COLUMN_DATE)
        save_charts(fig, "changepoints")
        plt.close(fig)

        # deltas
        deltas = f_model.params["delta"].mean(0)
        fig = plt.figure(facecolor="w")
        ax = fig.add_subplot(111)
        ax.bar(range(len(deltas)), deltas)
        ax.grid(True, which="major", c="gray", ls="-", lw=1, alpha=0.2)
        ax.set_ylabel("Rate change")
        ax.set_xlabel("Potential changepoint")
        fig.tight_layout()
        save_charts(fig, "deltas")
        logging.info("Output chart files produced.")
        plt.close(fig)

    # Catch-all exception
    except Exception as e:
        logging.error("Could not generate charts!")
        logging.error(str(e))

    pass

def save_charts(chart_object, chart_name):
    """
    Save charts as PNG file in the file storage
    """

    f_name = ("prophet_"+chart_name+"_"+str(DATE_SNIP)+".png")
    chart_file = ("/data/out/files/"+f_name)
    manifest_file = (chart_file+".manifest")
    logging.debug("Output PNG file: "+f_name)
    logging.debug("Output manifest file: "+f_name+".manifest")

    try:
        # Chart generation
        chart_object.savefig(chart_file)
        logging.debug("Output PNG file produced.")
        
        # Manifest assembly
        manifest = {
            "is_public": False,
            "is_permanent": False,
            "is_encrypted": True,
            "notify": False,
            "tags": [
                "forecast",
                "prophet",
                "chart"
            ]
        }

        try:
            with open(manifest_file, "w", encoding="utf-8") as manifest_out:
                json.dump(manifest, manifest_out)
            manifest_out.close()
            logging.debug("Output manifest file produced.")
        except:
            logging.error("Could not produce manifest file.")
    except:
        logging.error("Could not produce output PNG file.")



if __name__ == "__main__":
    """
    Main execution script.
    """

    #debug mode validation and logging level
    if DEBUG == True:
        logging.info("Debug mode selected, more information will be printed out.")
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        DEBUG == False
    
    # Supress futurewarnings (pystan and others)
    warnings.simplefilter(action='ignore', category=FutureWarning)
    warnings.filterwarnings("ignore",category=matplotlib.cbook.mplDeprecation)
    logging.info("Effective logging level is {}".format(
    logging.getLevelName(logger.getEffectiveLevel())))

    # determine data destination
    file_in, file_out = load_tables(in_tables, out_tables)

    # Get data
    model = input_file(file_in)

    # Perform model and param tests
    specs_valid = test_model(model, specs)

    # Perform prediction
    output_model = prediction(model, specs_valid)

    # Save data as CSV
    output_file(output_model, file_out)

    # Script done
    logging.info("Script completed.")
